# Swagger Employee CRUD

## Overview

CRUD b�sico que se comunica com um banco de dados MySql atrav�s do hibernate para fazer a gest�o de Employees.
Os endpoints desse projeto foram gerados usando o swagger codegen.
Para rodar o c�digo, voc� vai precisar de um arquivo hibernate.properties na pasta /conf contendo:


- hibernate.connection.url
- hibernate.connection.password
- hibernate.connection.username


Para rodar o servidor de testes, basta baixar as depend�ncias do pom.xml e executar o Servidor.java.


Todos os endpoints est�o contidos em:

```
http://localhost:8080/employee
```


Para mais detalhes, vide o swagger.yaml em /resources.