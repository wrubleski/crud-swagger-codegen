package br.com.lhenrique.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;

import org.hibernate.ObjectNotFoundException;

import br.com.lhenrique.db.EmployeeDatabaseService;
import br.com.lhenrique.model.Employee;
import br.com.lhenrique.model.ErrorModel;
import br.com.lhenrique.model.FieldLengthException;
import br.com.lhenrique.model.MissingRequiredFieldException;

// TODO: Use the exception mapper to handle exceptions

public class EmployeeController {
	public Response root_postRequest(Employee employee) {
		try {

			employee.validate();
			EmployeeDatabaseService db = new EmployeeDatabaseService();
			Employee addedEmployee = db.addEmployee(employee);
			return Response.status(201).entity(addedEmployee).build();
		} catch (FieldLengthException e) {
			return Response.status(400).entity(new ErrorModel(e.getMessage())).build();
		} catch (MissingRequiredFieldException e) {
			return Response.status(400).entity(new ErrorModel(e.getMessage())).build();
		}

		catch (Exception e) {
			System.out.println(e);
			return Response.status(500).entity(new ErrorModel("Internal Server Error")).build();
		}
	}

	public Response root_getRequest() {
		List<Employee> list;
		try {
			EmployeeDatabaseService db = new EmployeeDatabaseService();
			list = db.getAll();
			return Response.ok().entity(list).build();
		} catch (Exception e) {
			System.out.println("Internal Server Error");
			return Response.status(500).entity(new ErrorModel("Internal Server Error")).build();
		}
	}

	public Response root_id_getRequest(String id) {
		Employee emp;
		try {
			EmployeeDatabaseService db = new EmployeeDatabaseService();
			emp = db.getById(Integer.parseInt(id));
			return Response.ok().entity(emp).build();
		} catch (NoResultException e) {
			return Response.status(404).entity(new ErrorModel("No employee with such id found")).build();
		} catch (NumberFormatException e) {
			return Response.status(500).entity(new ErrorModel(
					"Invalid id. Path param id must be an integer. Make sure there's no letter or symbol in the path, and that id < 2147483647 (integer max value)."))
					.build();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Internal Server Error");
			return Response.status(500).entity(new ErrorModel("Internal Server Error")).build();
		}
	}

	public Response root_id_putRequest(Employee employee, String id) {
		try {
			employee.validate();
			EmployeeDatabaseService db = new EmployeeDatabaseService();
			Employee updatedEmployee = db.updateEmployee(Integer.parseInt(id), employee);
			return Response.ok().entity(updatedEmployee).build();
		} catch (FieldLengthException e) {
			return Response.status(400).entity(new ErrorModel(e.getMessage())).build();
		} catch (MissingRequiredFieldException e) {
			return Response.status(400).entity(new ErrorModel(e.getMessage())).build();
		} catch (NumberFormatException e) {
			return Response.status(400).entity(new ErrorModel(
					"Invalid id. Path param id must be an integer. Make sure there's no letter or symbol in the path, and that id < 2147483647."))
					.build();
		} catch (ObjectNotFoundException e) {
			return Response.status(404).entity(new ErrorModel("No employee with such id found")).build();
		} catch (InvocationTargetException e) {
			return Response.status(404).entity(new ErrorModel("No employee with such id found")).build();
		} catch (Exception e) {
			System.out.println(e);
			return Response.status(500).entity(new ErrorModel("Internal Server Error")).build();
		}
	}

	public Response root_id_deleteRequest(String id) {
		try {
			EmployeeDatabaseService db = new EmployeeDatabaseService();
			db.deleteEmployee(Integer.parseInt(id));
			return Response.status(204).build();
		} catch (NoResultException e) {
			return Response.status(404).entity(new ErrorModel("No employee with such id found.")).build();
		} catch (NumberFormatException e) {
			return Response.status(400).entity(new ErrorModel(
					"Invalid id. Path param id must be an integer. Make sure there's no letter or symbol in the path, and that id < 2147483647."))
					.build();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Internal Server Error");
			return Response.status(500).entity(new ErrorModel("Internal Server Error.")).build();
		}
	}
}
