package br.com.lhenrique.db;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import br.com.lhenrique.model.Employee;

public class EmployeeDatabaseService {

	public List<Employee> getAll() {
		DatabaseClient db = new DatabaseClient();
		List<Employee> employees = db.getAll("Employee");
		return employees;
	}

	public Employee getById(int id) {
		DatabaseClient db = new DatabaseClient();
		Employee employee = db.getById(id, "Employee", "employee_id");
		return employee;
	}

	public Employee addEmployee(Employee emp) {
		DatabaseClient db = new DatabaseClient();
		int userID = db.addElement(emp);
		return getById(userID);
	}

	public void deleteEmployee(int id) {
		Employee toBeDeleted = getById(id);
		DatabaseClient db = new DatabaseClient();
		db.deleteElement(toBeDeleted);
	}

	public Employee updateEmployee(int id, Employee data) throws IllegalAccessException, InvocationTargetException {
		DatabaseClient db = new DatabaseClient();
		db.updateElement(id, data);
		return getById(id);
	}
}
