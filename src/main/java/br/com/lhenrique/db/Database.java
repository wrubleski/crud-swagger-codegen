package br.com.lhenrique.db;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface Database {
	public <T> List<T> getAll(String table);

	public <T> T getById(int id, String table, String column);

	public int addElement(Object element);

	public void deleteElement(Object element);

	public <T> void updateElement(int id, T element) throws IllegalAccessException, InvocationTargetException;

}
