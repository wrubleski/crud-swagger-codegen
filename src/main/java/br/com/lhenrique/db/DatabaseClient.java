package br.com.lhenrique.db;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class DatabaseClient implements Database {
	private DatabaseConnection cnc = new DatabaseConnection();

	@Override
	public <T> List<T> getAll(String table) {
		Session session = cnc.makeSession();

		@SuppressWarnings("unchecked")
		Query<T> query = session.createQuery("from " + table);
		List<T> list = query.list();
		cnc.closeConnection();

		return list;

	}

	@Override
	public <T> T getById(int id, String table, String column) {

		Session session = cnc.makeSession();

		@SuppressWarnings("unchecked")
		Query<T> query = session.createQuery("from " + table + " where " + column + " = :id");
		query.setParameter("id", id);

		T result = query.getSingleResult();
		cnc.closeConnection();

		return result;
	}

	@Override
	public int addElement(Object element) {
		Session session = cnc.makeSession();
		session.beginTransaction();

		int userID = (Integer) session.save(element);
		session.getTransaction().commit();
		cnc.closeConnection();

		return userID;
	}

	@Override
	public void deleteElement(Object element) {
		Session session = cnc.makeSession();
		session.beginTransaction();
		session.delete(element);
		session.getTransaction().commit();
		cnc.closeConnection();
	}

	@Override
	public <T> void updateElement(int id, T element) throws IllegalAccessException, InvocationTargetException {

		Session session = cnc.makeSession();

		@SuppressWarnings("unchecked")
		T toBeUpdated = (T) session.load(element.getClass(), id);

		BeanUtils.copyProperties(toBeUpdated, element);

		session.beginTransaction();

		session.update(toBeUpdated);
		session.getTransaction().commit();
		cnc.closeConnection();
	}
}
