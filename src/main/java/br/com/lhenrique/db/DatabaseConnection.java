package br.com.lhenrique.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class DatabaseConnection {
	private StandardServiceRegistry ssr;
	private Metadata meta;

	private SessionFactory factory;
	private Session session;

	public Session makeSession() {
		ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		meta = new MetadataSources(ssr).getMetadataBuilder().build();
		factory = meta.getSessionFactoryBuilder().build();
		session = factory.openSession();

		return session;
	}

	public void closeConnection() {
		factory.close();
		session.close();
	}
}
