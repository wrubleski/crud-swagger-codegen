package br.com.lhenrique.handler;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.core.JsonParseException;

import br.com.lhenrique.model.ErrorModel;

@Provider
public class JsonParseExceptionMapper implements ExceptionMapper<JsonParseException> {
	public Response toResponse(JsonParseException ex) {
		return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorModel(ex.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
