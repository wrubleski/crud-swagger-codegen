package br.com.lhenrique.handler;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import br.com.lhenrique.model.ErrorModel;


@Provider
public class UnrecognizedPropertyExceptionMapper implements ExceptionMapper<UnrecognizedPropertyException> {
	public Response toResponse(UnrecognizedPropertyException ex) {
		System.out.println("GenericExceptionMapper");
		ex.printStackTrace();
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorModel(ex.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}
}