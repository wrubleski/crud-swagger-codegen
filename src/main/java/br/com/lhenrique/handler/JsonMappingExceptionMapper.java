package br.com.lhenrique.handler;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.lhenrique.model.ErrorModel;

@Provider
public class JsonMappingExceptionMapper implements ExceptionMapper<JsonMappingException> {
	public Response toResponse(JsonMappingException ex) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorModel(ex.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}

}