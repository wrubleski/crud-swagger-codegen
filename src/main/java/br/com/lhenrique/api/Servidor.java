package br.com.lhenrique.api;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.CommonProperties;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import br.com.lhenrique.handler.GenericExceptionMapper;
import br.com.lhenrique.handler.JsonMappingExceptionMapper;
import br.com.lhenrique.handler.JsonParseExceptionMapper;
import br.com.lhenrique.handler.UnrecognizedPropertyExceptionMapper;

public class Servidor {

	public static void main(String[] args) {
		try {
			URI uri = UriBuilder.fromUri("http://localhost").port(8080).build();
			ResourceConfig config = new ResourceConfig();
			config.register(JacksonFeature.class);
			config.register(JsonMappingExceptionMapper.class);
			config.register(UnrecognizedPropertyExceptionMapper.class);
			config.register(GenericExceptionMapper.class);
			config.register(JsonParseExceptionMapper.class);
			config.packages("br.com.lhenrique.api");
			config.property(CommonProperties.JSON_PROCESSING_FEATURE_DISABLE, true);
			GrizzlyHttpServerFactory.createHttpServer(uri, config);
			System.out.println("Listening on port 8080");
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("Erro na execucao: " + e.getMessage());
		}

	}

}
