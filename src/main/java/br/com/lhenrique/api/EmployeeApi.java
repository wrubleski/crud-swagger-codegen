package br.com.lhenrique.api;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.lhenrique.controller.EmployeeController;
import br.com.lhenrique.model.Employee;
import br.com.lhenrique.model.ErrorModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/employee")
@Api(description = "the employee API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2021-06-01T15:18:40.383-03:00[America/Sao_Paulo]")
public class EmployeeApi {
	private EmployeeController controller = new EmployeeController();

	@POST
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "Insert an employee into the database", response = Employee.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Return the added employee", response = Employee.class),
			@ApiResponse(code = 500, message = "Unexpected error", response = ErrorModel.class),
			@ApiResponse(code = 400, message = "Validation failed", response = ErrorModel.class) })
	public Response addEmployee(@Valid Employee employee) {
		return controller.root_postRequest(employee);
	}

	@DELETE
	@Path("/{id}")
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "deletes a single employee based on the ID supplied", response = Void.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Employee deleted successfully", response = Void.class),
			@ApiResponse(code = 404, message = "no employee with such id found", response = ErrorModel.class),
			@ApiResponse(code = 500, message = "unexpected error", response = ErrorModel.class) })
	public Response deleteEmployee(@PathParam("id") @ApiParam("ID of the employee to be deleted") String id) {

		return controller.root_id_deleteRequest(id);
	}

	@PUT
	@Path("/{id}")
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "updates a single employee information based on the ID and employee supplied. If no value is provided, property is set to NULL.", response = Employee.class, tags = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Employee updated successfully", response = Employee.class),
			@ApiResponse(code = 404, message = "no employee with such id found", response = ErrorModel.class),
			@ApiResponse(code = 500, message = "unexpected error", response = ErrorModel.class) })
	public Response updateEmployee(@Valid Employee employee,
			@PathParam("id") @ApiParam("ID of the employee") String id) {
		return controller.root_id_putRequest(employee, id);
	}

	@GET
	@Path("/{id}")
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "find and return the employee by ID", response = Employee.class, tags = {})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "response OK", response = Employee.class),
			@ApiResponse(code = 500, message = "unexpected error", response = ErrorModel.class),
			@ApiResponse(code = 404, message = "no employee with such id found", response = ErrorModel.class) })
	public Response findEmployee(@PathParam("id") @ApiParam("ID of the employee") String id) {
		return controller.root_id_getRequest(id);
	}

	@GET
	@Produces({ "application/json" })
	@ApiOperation(value = "", notes = "returns all the employees", response = Employee.class, responseContainer = "List", tags = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "response OK", response = Employee.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "unexpected error", response = ErrorModel.class) })
	public Response getEmployees() {
		return controller.root_getRequest();
	}
}
