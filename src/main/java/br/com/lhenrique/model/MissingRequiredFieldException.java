package br.com.lhenrique.model;

public class MissingRequiredFieldException extends RuntimeException {
	public MissingRequiredFieldException(String message) {
		super(message);
	}

}
