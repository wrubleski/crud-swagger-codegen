package br.com.lhenrique.model;

public class FieldLengthException extends RuntimeException {
	public FieldLengthException(String message) {
		super(message);
	}
}
