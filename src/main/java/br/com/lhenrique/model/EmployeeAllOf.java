package br.com.lhenrique.model;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2021-06-01T15:18:40.383-03:00[America/Sao_Paulo]")
public class EmployeeAllOf {

	private @Valid Integer employeeId;

	/**
	 **/
	public EmployeeAllOf employeeId(Integer employeeId) {
		this.employeeId = employeeId;
		return this;
	}

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("employee_id")
	@NotNull
	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		EmployeeAllOf employeeAllOf = (EmployeeAllOf) o;
		return Objects.equals(this.employeeId, employeeAllOf.employeeId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(employeeId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EmployeeAllOf {\n");

		sb.append("    employeeId: ").append(toIndentedString(employeeId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
