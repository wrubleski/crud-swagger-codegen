package br.com.lhenrique.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "employees")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2021-06-01T15:18:40.383-03:00[America/Sao_Paulo]")
public class Employee {
	@Column(name = "first_name", length = 45)
	private @Valid String firstName;
	@Column(name = "last_name", length = 45)
	private @Valid String lastName;
	@Column(name = "department", length = 45)
	private @Valid String department;
	@Column(name = "job_title", length = 45)
	private @Valid String jobTitle;
	@Column(name = "employee_type", length = 45)
	private @Valid String employeeType;
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private @Valid Date startDate;
	@Column(name = "status", length = 45)
	private @Valid String status;
	@Column(name = "email", length = 45)
	private @Valid String email;
	@Id
	@Column(name = "employee_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @Valid Integer employeeId;

	public Employee() {

	}

	/**
	 **/
	public Employee firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("first_name")
	@Size(max = 45)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	   **/
	public Employee lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("last_name")
	@Size(max = 45)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	   **/
	public Employee department(String department) {
		this.department = department;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("department")
	@Size(max = 45)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	   **/
	public Employee jobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("job_title")
	@Size(max = 45)
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	   **/
	public Employee employeeType(String employeeType) {
		this.employeeType = employeeType;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("employee_type")
	@Size(max = 45)
	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	/**
	   **/
	public Employee startDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("start_date")
	@Size(max = 10)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	   **/
	public Employee status(String status) {
		this.status = status;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("status")
	@Size(max = 45)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	   **/
	public Employee email(String email) {
		this.email = email;
		return this;
	}

	@ApiModelProperty(value = "")
	@JsonProperty("email")
	@Size(max = 45)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	   **/
	public Employee employeeId(Integer employeeId) {
		this.employeeId = employeeId;
		return this;
	}

	@ApiModelProperty(required = true, value = "")
	@JsonProperty("employee_id")
	@NotNull
	public Integer getEmployeeId() {
		return employeeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Employee employee = (Employee) o;
		return Objects.equals(this.firstName, employee.firstName) && Objects.equals(this.lastName, employee.lastName)
				&& Objects.equals(this.department, employee.department)
				&& Objects.equals(this.jobTitle, employee.jobTitle)
				&& Objects.equals(this.employeeType, employee.employeeType)
				&& Objects.equals(this.startDate, employee.startDate) && Objects.equals(this.status, employee.status)
				&& Objects.equals(this.email, employee.email) && Objects.equals(this.employeeId, employee.employeeId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, department, jobTitle, employeeType, startDate, status, email,
				employeeId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Employee {\n");

		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    department: ").append(toIndentedString(department)).append("\n");
		sb.append("    jobTitle: ").append(toIndentedString(jobTitle)).append("\n");
		sb.append("    employeeType: ").append(toIndentedString(employeeType)).append("\n");
		sb.append("    startDate: ").append(toIndentedString(startDate)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    employeeId: ").append(toIndentedString(employeeId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public void validate() {

//		@Column(name = "start_date")
//		@Temporal(TemporalType.DATE)
//		private @Valid Date startDate;

//		@Id
//		@Column(name = "employee_id")
//		@GeneratedValue(strategy = GenerationType.IDENTITY)
//		private @Valid Integer employeeId;

		String error = "Validation exception: ";
		int errorCounter = 0;

		try {
			if (this.firstName.length() > 45) {
				error = error + "first_name max length exceded. ";
				errorCounter += 1;
			}

			if (this.lastName.length() > 45) {
				error = error + "last_name max length exceded. ";
				errorCounter += 1;
			}

			if (this.department.length() > 45) {
				error = error + "department max length exceded. ";
				errorCounter += 1;
			}

			if (this.jobTitle.length() > 45) {
				error = error + "job_title max length exceded. ";
				errorCounter += 1;
			}
			if (this.employeeType.length() > 45) {
				error = error + "employee_typemax length exceded. ";
				errorCounter += 1;
			}

			if (this.status.length() > 45) {
				error = error + "status max length exceded. ";
				errorCounter += 1;
			}

			if (this.email.length() > 45) {
				error = error + "email max length exceded. ";
				errorCounter += 1;
			}

		} catch (NullPointerException e) {
			throw new MissingRequiredFieldException("Employee missing required properties.");
		}

//		if (this.startDate.length() > 10) {
//			error = error + "email max length exceded.";
//			errorCounter += 1;
//		}

		if (errorCounter > 0)
			throw new FieldLengthException(error.trim());
	}

}
